import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Injectable({providedIn: 'root'})
export class ApiCallService {

  private apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  private makeGetCall(url) {
    return this.httpClient.get(url, {headers: new HttpHeaders({
          'Content-Type':  'application/json'
        })
      });
  }

  private makePostCall(url, body) {
    return this.httpClient.post(url, body, {headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    });
  }

  getThings() {
    const prefix = '/thing';
    return this.makeGetCall(`${this.apiUrl}${prefix}`);
  }

  postThing(body) {
    const prefix = '/thing';
    return this.makePostCall(`${this.apiUrl}${prefix}`, body);
  }
}
