import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListThingComponent } from './thing/list-thing/list-thing.component';
import { PathNotFoundComponent } from './path-not-found/path-not-found.component';
import { CreateThingComponent } from './thing/create-thing/create-thing.component';

const routes: Routes = [
  { path: '', redirectTo: 'thing', pathMatch: 'full' },
  { path: 'thing', component: ListThingComponent },
  { path: 'thing/create', component: CreateThingComponent },
  { path: '**', component: PathNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
