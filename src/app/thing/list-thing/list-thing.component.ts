import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Thing } from 'src/app/interfaces/Thing';
import { ApiCallService } from 'src/app/services/apicall.service';

@Component({
  selector: 'app-list-thing',
  templateUrl: './list-thing.component.html',
  styleUrls: ['./list-thing.component.css']
})
export class ListThingComponent implements OnInit {

  dataSource: Thing[] = [
    { id: 10, price: 200, category: 1 },
    { id: 10, price: 200, category: 1 },
    { id: 10, price: 200, category: 1 },
    { id: 10, price: 200 },
    { id: 10, price: 200 },
    { id: 10, price: 200 },
  ];

  displayedColumns: string[] = ['id', 'price', 'category'];

  listForm = this.fb.group({
    price: this.fb.array([])
  });

  constructor(private fb: FormBuilder, private as: ApiCallService) { }

  changeThing(val) {
    console.log(val);
  }

  ngOnInit() {
    // this.as.getThings().subscribe((val: Thing[]) => {
    //   this.dataSource = val;
    // });
  }


}
